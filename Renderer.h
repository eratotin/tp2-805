/**
   @file Renderer.h
   @author JOL
*/
#pragma once
#ifndef _RENDERER_H_
#define _RENDERER_H_

#include "Color.h"
#include "Image2D.h"
#include "Ray.h"


/// Namespace RayTracer
namespace rt {

    inline void progressBar( std::ostream& output,
                             const double currentValue, const double maximumValue)
    {
        static const int PROGRESSBARWIDTH = 60;
        static int myProgressBarRotation = 0;
        static int myProgressBarCurrent = 0;
        // how wide you want the progress meter to be
        double fraction = currentValue /maximumValue;
    
        // part of the progressmeter that's already "full"
        int dotz = static_cast<int>(floor(fraction * PROGRESSBARWIDTH));
        if (dotz > PROGRESSBARWIDTH) dotz = PROGRESSBARWIDTH;
    
        // if the fullness hasn't changed skip display
        if (dotz == myProgressBarCurrent) return;
        myProgressBarCurrent = dotz;
        myProgressBarRotation++;
    
        // create the "meter"
        int ii=0;
        output << "[";
        // part  that's full already
        for ( ; ii < dotz;ii++) output<< "#";
        // remaining part (spaces)
        for ( ; ii < PROGRESSBARWIDTH;ii++) output<< " ";
        static const char* rotation_string = "|\\-/";
        myProgressBarRotation %= 4;
        output << "] " << rotation_string[myProgressBarRotation]
               << " " << (int)(fraction*100)<<"/100\r";
        output.flush();
    }

    struct Background {
        virtual Color backgroundColor( const Ray& ray ) = 0;
    };

    struct MyBackground : public Background {
        Color backgroundColor( const Ray& ray ){
            Real comp_z = ray.direction[2];
            if(0 <= comp_z && comp_z < 0.5){
                return Color(1.0f - 2.0f * comp_z, 1.0f - 2.0f * comp_z, 1.0f);
            }else if(comp_z >= 0.5f && comp_z < 1.0f){
                return Color(0.0f, 0.0f, 1.0f + (comp_z /*- 0.5f*/) * 2.0f);
            }else{
                return Color(0.0,0.0,0.0);
            }
        }
    };
  
    /// This structure takes care of rendering a scene.
    struct Renderer {

        /// The scene to render
        Scene* ptrScene;
        // On rajoute un pointeur vers un objet Background
        Background* ptrBackground;
        /// The origin of the camera in space.
        Point3 myOrigin;
        /// (myOrigin, myOrigin+myDirUL) forms a ray going through the upper-left
        /// corner pixel of the viewport, i.e. pixel (0,0)
        Vector3 myDirUL;
        /// (myOrigin, myOrigin+myDirUR) forms a ray going through the upper-right
        /// corner pixel of the viewport, i.e. pixel (width,0)
        Vector3 myDirUR;
        /// (myOrigin, myOrigin+myDirLL) forms a ray going through the lower-left
        /// corner pixel of the viewport, i.e. pixel (0,height)
        Vector3 myDirLL;
        /// (myOrigin, myOrigin+myDirLR) forms a ray going through the lower-right
        /// corner pixel of the viewport, i.e. pixel (width,height)
        Vector3 myDirLR;

        int myWidth;
        int myHeight;

    Renderer() : ptrScene( 0 ) {}

    Renderer( Scene& scene, Background& b ) : ptrScene( &scene ), ptrBackground (&b) {}

        void setScene( rt::Scene& aScene ) { ptrScene = &aScene; }
    
        void setViewBox( Point3 origin, 
                         Vector3 dirUL, Vector3 dirUR, Vector3 dirLL, Vector3 dirLR )
        {
            myOrigin = origin;
            myDirUL = dirUL;
            myDirUR = dirUR;
            myDirLL = dirLL;
            myDirLR = dirLR;
        }

        void setResolution( int width, int height )
        {
            myWidth  = width;
            myHeight = height;
        }

        // Affiche les sources de lumières avant d'appeler la fonction qui
        // donne la couleur de fond.
        Color background( const Ray& ray )
        {
            Color result = Color( 0.0, 0.0, 0.0 );
            for ( Light* light : ptrScene->myLights )
                {
                    Real cos_a = light->direction( ray.origin ).dot( ray.direction );
                    if ( cos_a > 0.99f )
                        {
                            Real a = acos( cos_a ) * 360.0 / M_PI / 8.0;
                            a = std::max( 1.0f - a, 0.0f );
                            result += light->color( ray.origin ) * a * a;
                        }
                }
            if(ray.direction[2] < 0.0f){
                Real x = -0.5f * ray.direction[ 0 ] / ray.direction[ 2 ];
                Real y = -0.5f * ray.direction[ 1 ] / ray.direction[ 2 ];
                Real d = sqrt( x*x + y*y );
                Real t = std::min( d, 30.0f ) / 30.0f;
                x -= floor( x );
                y -= floor( y );
                if ( ( ( x >= 0.5f ) && ( y >= 0.5f ) ) || ( ( x < 0.5f ) && ( y < 0.5f ) ) )
                    result += (1.0f - t)*Color( 0.2f, 0.2f, 0.2f ) + t * Color( 1.0f, 1.0f, 1.0f );
                else
                    result += (1.0f - t)*Color( 0.4f, 0.4f, 0.4f ) + t * Color( 1.0f, 1.0f, 1.0f );
            }
            if ( ptrBackground != 0 ) result += ptrBackground->backgroundColor( ray );
            return result;
        }


        /// The main rendering routine
        void render( Image2D<Color>& image, int max_depth )
        {
            std::cout << "Rendering into image ... might take a while." << std::endl;
            image = Image2D<Color>( myWidth, myHeight );
            for ( int y = 0; y < myHeight; ++y ) 
                {
                    Real    ty   = (Real) y / (Real)(myHeight-1);
                    progressBar( std::cout, ty, 1.0 );
                    Vector3 dirL = (1.0f - ty) * myDirUL + ty * myDirLL;
                    Vector3 dirR = (1.0f - ty) * myDirUR + ty * myDirLR;
                    dirL        /= dirL.norm();
                    dirR        /= dirR.norm();
                    for ( int x = 0; x < myWidth; ++x ) 
                        {
                            Real    tx   = (Real) x / (Real)(myWidth-1);
                            Vector3 dir  = (1.0f - tx) * dirL + tx * dirR;
                            Ray eye_ray  = Ray( myOrigin, dir, max_depth );
                            Color result = trace( eye_ray );
                            image.at( x, y ) = result.clamp();
                        }
                }
            std::cout << "Done." << std::endl;
        }


        /// The rendering routine for one ray.
        /// @return the color for the given ray.
        Color trace( const Ray& ray )
        {
            assert( ptrScene != 0 );
            GraphicalObject* obj_i = 0; // pointer to intersected object
            Point3           p_i;       // point of intersection

            // Look for intersection in this direction.
            Real ri = ptrScene->rayIntersection( ray, obj_i, p_i );
            // Nothing was intersected
            if ( ri >= 0.0f ) return background(ray); // some background color

            Color C(0.0f,0.0f,0.0f);
            Material m = obj_i->getMaterial(p_i);
            if (ray.depth > 0 && m.coef_reflexion != 0.0f){
                Ray ray_refl(p_i, reflect(ray.direction, obj_i->getNormal(p_i)), ray.depth - 1);
                Color c_refl = trace(ray_refl);
                C += c_refl * m.specular * m.coef_reflexion;
            }

            if (ray.depth > 0 && m.coef_refraction != 0.0f){
                C += trace(refractionRay(ray, p_i, obj_i->getNormal(p_i), m));
            }
            return C + illumination(ray, obj_i, p_i);
            //return obj_i->getMaterial(p_i).diffuse + obj_i->getMaterial(p_i).ambient;
        }

        Ray refractionRay( const Ray& aRay, const Point3& p, Vector3 N, const Material& m ){
            Real c = aRay.direction.dot(N);
            if (c > 0){
                Real r = m.out_refractive_index / m.in_refractive_index;
                Vector3 v_refract = r * aRay.direction + (Real)((r * c - sqrt(1 - r * r * (1 - c * c)))) * N;
                Ray r_refract(p,v_refract);
                return r_refract;
            }else{
                c = -c;
                Real r = m.in_refractive_index / m.out_refractive_index;
                Vector3 v_refract = r * aRay.direction + (Real)((r * c - sqrt(1 - r * r * (1 - c * c)))) * N;
                Ray r_refract(p,v_refract, aRay.depth - 1);
                return r_refract;
            }
        }

        /// Calcule le vecteur réfléchi à W selon la normale N.
        Vector3 reflect( const Vector3& W, Vector3 N ) const{
            Vector3 u = W.dot(N) * N;
            Vector3 l = W - u;
            /*miroir = l - u;*/
            return l - u;
        }
      
        Color shadow(const Ray& ray, Color light_color){
            Color c = light_color;
            Point3 p; 
            GraphicalObject* go;
            Ray ray2( ray );
            // ptrScene->rayIntersection(ray2, go, p);
            while( c.max() > 0.003f){
                ray2.origin += 0.0001f * ray.direction;
                if (ptrScene->rayIntersection(ray2, go, p) < 0.0 ){
                    c = c * go->getMaterial(p).diffuse * go->getMaterial(p).coef_refraction;
                    ray2.origin = p;
                }else{
                    break;
                }
            }
            return c;
        }

        


        /// Calcule l'illumination de l'objet \a obj au point \a p, sachant que l'observateur est le rayon \a ray.
        Color illumination( const Ray& ray, GraphicalObject* obj, Point3 p ){
            Vector3 normal = obj->getNormal(p);
            Vector3 miroir = reflect(ray.direction,normal);
            Real cosb;
            Real spec;
            Material m = obj->getMaterial(p);
            Color c (0.0f,0.0f,0.0f );
            for(Light* light : ptrScene->myLights){
                Vector3 v = light->direction(p);
                Real coef_diff = v.dot(normal); ///normal.norm();
                Color SC = shadow(Ray(p,v), light->color(p));
                //Color SC = light->color(p);
                if (coef_diff > 0){
                    c += SC * m.diffuse * coef_diff * m.coef_diffusion;
                }
                cosb = miroir.dot(v);
                if(cosb > 0){
                    spec = pow(cosb,m.shinyness);
                    c += SC * m.specular * ( spec * m.coef_reflexion );
                }
            }
            c += m.ambient;
            return c;
        }

    };

} // namespace rt

#endif // #define _RENDERER_H_
